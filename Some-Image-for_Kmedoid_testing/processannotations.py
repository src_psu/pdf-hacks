import sys
import xml.etree.ElementTree as ET

inpfile=sys.argv[1]
tree = ET.parse(inpfile)

outpfile=open(inpfile.split(".xm")[0]+"-mod.xml","w")

objects=[]

root=tree.getroot()

for child in root:
   if (child.tag=='object'):
	objects.append(child)

for item in objects:
   tagname=item.find('name').text
   points=item.find('polygon').findall('pt')
   xs=[]
   ys=[]
   output=[]
   output.append(tagname)
   for point in points:
	xs.append(int(point.find('x').text))
	ys.append(int(point.find('y').text))
   output.append(str(min(xs)))
   output.append(str(min(ys)))
   output.append(str(max(xs)))
   output.append(str(max(ys)))
   
   outpfile.write(str(output)+"\n")

outpfile.close()
   

