package at.ac.tuwien.dbai.pdfwrap;

import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.util.PDFTextStripperByArea;

import at.ac.tuwien.dbai.pdfwrap.model.document.ImageSegment;
import at.ac.tuwien.dbai.pdfwrap.model.document.LineSegment;
import at.ac.tuwien.dbai.pdfwrap.model.document.Page;
import at.ac.tuwien.dbai.pdfwrap.model.document.RectSegment;
import at.ac.tuwien.dbai.pdfwrap.model.document.TextBlock;

public class OnePageHelper {

	static float POINTTOPIXELCONSTANT=(float) 2.083; //point to pixel conversion: divide density paramerter 
	//in convert command (CreateImages.java) by 72


	static void getBoxLocations(
			float[] pageBoundingBox, String inFile, List<ImageSegment> imageSegments, List<LineSegment> lineSegments,
			List<RectSegment> rectSegments, Page resultPage, int currentPage) throws IOException {

		
		PrintWriter writer = new PrintWriter(inFile.substring(0,inFile.length()-4)+"-boxlocs", "UTF-8");
		List<String> boundingBoxes=new ArrayList<String>();
		List<ArrayList<Float>> nonTextBoxes = new ArrayList<ArrayList<Float>>();
		
		PDDocument document = PDDocument.load(new File(inFile));
		PDDocumentCatalog catalog = document.getDocumentCatalog();
	    @SuppressWarnings("unchecked")
	    List<PDPage> pages = catalog.getAllPages();
	    PDPage resultPDPage=pages.get(currentPage);
	    //this needs to be optimized //
	    //------------------------------------------------------------------//
	    for (int i=0; i< imageSegments.size(); i++ ){
			ArrayList<Float> temp = new ArrayList<Float>();
			float x1=imageSegments.get(i).getX1();
			float x2=imageSegments.get(i).getX2();
			float y1=imageSegments.get(i).getY1();
			float y2=imageSegments.get(i).getY2();
			if (x1>x2){
				float t=x2;
				x2=x1;
				x1=t;
			}
			
			if (y2>y1){
				float t=y2;
				y2=y1;
				y1=t;
			}
			if (x1<2) x1=2; if (y1<2) y1=2;
			temp.add(x1);temp.add(y1);temp.add(x2);temp.add(y2);
			nonTextBoxes.add(temp);
	    }
	    
	    for (int i=0; i< lineSegments.size(); i++ ){
			ArrayList<Float> temp = new ArrayList<Float>();
			float x1=lineSegments.get(i).getX1();
			float x2=lineSegments.get(i).getX2();
			float y1=lineSegments.get(i).getY1();
			float y2=lineSegments.get(i).getY2();
			if (x1>x2){
				float t=x2;
				x2=x1;
				x1=t;
			}
			if (y2>y1){
				float t=y2;
				y2=y1;
				y1=t;
			}
			if (x1<2) x1=2; if (y1<2) y1=2;
			temp.add(x1);temp.add(y1);temp.add(x2);temp.add(y2);
			nonTextBoxes.add(temp);
	    }
	    
	    for (int i=0; i< rectSegments.size(); i++ ){

			ArrayList<Float> temp = new ArrayList<Float>();
			float x1=rectSegments.get(i).getX1();
			float x2=rectSegments.get(i).getX2();
			float y1=rectSegments.get(i).getY1();
			float y2=rectSegments.get(i).getY2();
			if (x1>x2){
				float t=x2;
				x2=x1;
				x1=t;
			}
			if (y2>y1){
				float t=y2;
				y2=y1;
				y1=t;
			}
			if (x1<2) x1=2; if (y1<2) y1=2;
			temp.add(x1);temp.add(y1);temp.add(x2);temp.add(y2);
			nonTextBoxes.add(temp);
	    }
	    
	  //------------------------------------------------------------------//
		for (int i=0; i< imageSegments.size(); i++ ){
			ArrayList<Float> temp = new ArrayList<Float>();
			float x1=imageSegments.get(i).getX1();
			float x2=imageSegments.get(i).getX2();
			float y1=imageSegments.get(i).getY1();
			float y2=imageSegments.get(i).getY2();
			if (x1>x2){
				float t=x2;
				x2=x1;
				x1=t;
			}
			if (y2>y1){
				float t=y2;
				y2=y1;
				y1=t;
			}
			temp.add(x1);temp.add(y1);temp.add(x2);temp.add(y2);
			//nonTextBoxes.add(temp);
			temp.add((float)0);
			boundingBoxes.add(convertToImageCoordinates(temp,resultPage.getHeight())+","
			+textDensityFeature(temp,resultPage.getHeight(),resultPDPage)+","
			+disTancefromBoundary(temp,resultPage.getWidth(),resultPage.getHeight())+","
			+epsDensity(temp,nonTextBoxes,resultPage.getHeight())+","+"0"+","
			+String.valueOf(Math.abs((x1-x2)*(y1-y2))));
			//System.out.println("non text box size "+nonTextBoxes.size());
		}
		for (int i=0; i< lineSegments.size(); i++ ){
			ArrayList<Float> temp = new ArrayList<Float>();
			float x1=lineSegments.get(i).getX1();
			float x2=lineSegments.get(i).getX2();
			float y1=lineSegments.get(i).getY1();
			float y2=lineSegments.get(i).getY2();
			if (x1>x2){
				float t=x2;
				x2=x1;
				x1=t;
			}
			if (y2>y1){
				float t=y2;
				y2=y1;
				y1=t;
			}
			temp.add(x1);temp.add(y1);temp.add(x2);temp.add(y2);
			//nonTextBoxes.add(temp);
			temp.add((float)1);
			boundingBoxes.add(convertToImageCoordinates(temp,resultPage.getHeight())+","
			+textDensityFeature(temp,resultPage.getHeight(),resultPDPage)+","
			+disTancefromBoundary(temp,resultPage.getWidth(),resultPage.getHeight())+","
			+epsDensity(temp,nonTextBoxes,resultPage.getHeight())+","+"1"+","
			+String.valueOf(Math.abs((x1-x2)*(y1-y2))));
			//System.out.println("non text box size "+nonTextBoxes.size());
		}

		for (int i=0; i< rectSegments.size(); i++ ){

			ArrayList<Float> temp = new ArrayList<Float>();
			float x1=rectSegments.get(i).getX1();
			float x2=rectSegments.get(i).getX2();
			float y1=rectSegments.get(i).getY1();
			float y2=rectSegments.get(i).getY2();
			if (x1>x2){
				float t=x2;
				x2=x1;
				x1=t;
			}
			if (y2>y1){
				float t=y2;
				y2=y1;
				y1=t;
			}
			temp.add(x1);temp.add(y1);temp.add(x2);temp.add(y2);
			//nonTextBoxes.add(temp);
			temp.add((float)2);
			//System.out.println("Added bounding box rectsegment "+temp);
			boundingBoxes.add(convertToImageCoordinates(temp,resultPage.getHeight())+","
			+textDensityFeature(temp,resultPage.getHeight(),resultPDPage)+","
			+disTancefromBoundary(temp, resultPage.getWidth(),resultPage.getHeight())+","
			+epsDensity(temp,nonTextBoxes,resultPage.getHeight())+","+"2"+","
			+String.valueOf(Math.abs((x1-x2)*(y1-y2))));
			//System.out.println("non text box size "+nonTextBoxes.size());
		}

		for (String s:boundingBoxes){
			writer.println(s);
		}
		writer.close();
	}


	private static String epsDensity(List<Float> thisBox,List<ArrayList<Float>> nonTextBoxes, float pageHeight) {
		/**
		 * This function returns number of boxes close to this box. scattered 
		 * boxes should be removed. 
		 */
		
		int pointsinEpsNeighborhood=0;
		int EpsNeighborhoodDist=20;
		
		//boolean check=false;
		
		ArrayList<Float> newthisBox = new ArrayList<Float>(thisBox);
		newthisBox.set(0, newthisBox.get(0)-2);newthisBox.set(1, pageHeight-(newthisBox.get(1)+2));
		newthisBox.set(2, newthisBox.get(2)+2);newthisBox.set(3, pageHeight-(newthisBox.get(3)-2));
		
		for (ArrayList<Float> nT:nonTextBoxes){
			ArrayList<Float> newnT = new ArrayList<Float>(nT);
			newnT.set(0, newnT.get(0)-2);newnT.set(1, pageHeight-(newnT.get(1)+2));
			newnT.set(2, newnT.get(2)+2);newnT.set(3, pageHeight-(newnT.get(3)-2));
			//if (check==true)
			//System.out.println(thisBox+":"+nT+":"+fastKmedoidClustering.calculateDistBetweenBoxes(thisBox, nT));
			if (fastKmedoidClustering.calculateDistBetweenBoxes(newthisBox, newnT)<EpsNeighborhoodDist)
				pointsinEpsNeighborhood+=1;
		}
		return String.valueOf(pointsinEpsNeighborhood-1);
	}


	private static String disTancefromBoundary(List<Float> temp, float width, float height) {
		/**
		 * This function returns the distance from nearest boundary of the box. lines
		 * at the left/right/top/bottm end should be removed. 
		 */
		// find the closest corner and get the manhattan distance from there.
		//remember this temp is in points and the origin of the co-ordinate system 
		//is top left corner, so we have to change the coordinates.
		float pageheight=height;
		float x1=temp.get(0);
		float y1=pageheight-temp.get(1);
		float x2=temp.get(2);
		float y2=pageheight-temp.get(3);

		List<Float> dists= new ArrayList<Float>();
		//top-left
		dists.add(Math.abs(x1-0));dists.add(Math.abs(y1-0));dists.add(Math.abs(x2-0));dists.add(Math.abs(y2-0));
		//top-right
		dists.add(Math.abs(x1-width));dists.add(Math.abs(y1-0));dists.add(Math.abs(x2-width));dists.add(Math.abs(y2-0));
		//bottom-left
		dists.add(Math.abs(x1-0));dists.add(Math.abs(y1-height));dists.add(Math.abs(x2-0));dists.add(Math.abs(y2-height));
		//top-right
		dists.add(Math.abs(x1-width));dists.add(Math.abs(y1-height));dists.add(Math.abs(x2-width));dists.add(Math.abs(y2-height));

		
		return String.valueOf(Math.round(Collections.min(dists)));
		
	}
    
	
	
	private static String textDensityFeature(List<Float> temp, float pageheight, PDPage resultPDPage) throws IOException {
		/**
		 * This function returns number of characters around the given bounding box. lines 
		 * coming from tables and stuff should be removed by this feature.
		 */
		// get the text from a bigger box enclosing the given boundingbox
		//remember this temp is in points and the origin of the co-ordinate system 
		//is top left corner, so we have to change the coordinates.
		float boxpadding=10;
		float x=temp.get(0)-boxpadding;
		float y=pageheight-temp.get(1)-boxpadding;
		float width=temp.get(2)-temp.get(0)+boxpadding;
		float height=Math.abs(temp.get(3)-temp.get(1))+boxpadding;
		
		Rectangle2D region = new Rectangle2D.Double(x, y, width, height);
		String regionName = "region";
		PDFTextStripperByArea stripper;

		stripper = new PDFTextStripperByArea();
		stripper.addRegion(regionName, region);
		try{
			stripper.extractRegions(resultPDPage);
			//System.out.println( "Text in the area:" + region);
			double extregion=region.getWidth()*region.getHeight();
			double thisregion=(width-boxpadding+2)*(height-boxpadding+2);
			//return String.valueOf((stripper.getTextForRegion( "region" ).trim().toCharArray().length)/Math.abs(extregion-thisregion));
			System.out.println(String.valueOf((stripper.getTextForRegion( "region" ).trim().toCharArray().length)));
			return String.valueOf((stripper.getTextForRegion( "region" ).trim().toCharArray().length));
			//return null;
		}catch(Exception e){
			return String.valueOf(2.5);
		}
	}


	private static String convertToImageCoordinates(List<Float> figureLocation, float pageheight) {
		String figureloc="";
		figureloc+=(int) (figureLocation.get(0)*POINTTOPIXELCONSTANT)+","; //this is x1, just multiply.
		figureloc+= (int) ((pageheight-figureLocation.get(1))*POINTTOPIXELCONSTANT)+","; //this is y1, subtract from page y first. 
		figureloc+= (int) (figureLocation.get(2)*POINTTOPIXELCONSTANT)+",";//this is x2, just multiply.
		figureloc+= (int) ((pageheight-figureLocation.get(3))*POINTTOPIXELCONSTANT); //this is y2, subtract from page y first.

		return figureloc;
	}

}