package at.ac.tuwien.dbai.pdfwrap;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;

public class ExtractTextPDFBox {
public static void main(String args[]) throws FileNotFoundException, IOException{
	PDFTextStripper pdfStripper = null;
	PDDocument pdDoc = null;
	COSDocument cosDoc = null;
	File file = new File(args[0]);

	PDFParser parser = new PDFParser(new FileInputStream(file));
	parser.parse();
	cosDoc = parser.getDocument();
	pdfStripper = new PDFTextStripper();
	pdDoc = new PDDocument(cosDoc);
	pdfStripper.setStartPage(1);
	pdfStripper.setEndPage(5);
	String parsedText = pdfStripper.getText(pdDoc);
	System.out.println(parsedText);
	
}
}
