package at.ac.tuwien.dbai.pdfwrap;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.image.CropImageFilter;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageProducer;
import java.awt.image.RenderedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import java.awt.image.FilteredImageSource;

import org.apache.commons.io.FileUtils;

import com.sun.media.jai.codecimpl.JPEGCodec;
public class CreateImages {

	public static boolean createPDFPageImages(String pdfloc,String pdfname){
		//assumes that there is a directory with the same name as the pdf.
		String newpdfloc=pdfloc.substring(0,pdfloc.length()-4)+"/"+pdfname;
		try{
			FileUtils.copyFile(new File(pdfloc), new File(newpdfloc));
		}catch(IOException e){
			System.out.println("Failed to copy file");
			return false;
		}
		String imageFilebase=newpdfloc.substring(0,newpdfloc.length()-4)+".png";
		String convertCommand="convert "+"-density 150 "+newpdfloc+" -quality 100 -sharpen 0x1.0 "+imageFilebase;

		String s=null;
		try {
			Process p = Runtime.getRuntime().exec(convertCommand);

			BufferedReader stdInput = new BufferedReader(new
					InputStreamReader(p.getInputStream()));

			BufferedReader stdError = new BufferedReader(new
					InputStreamReader(p.getErrorStream()));

			System.out.println("Here is the standard output of the command:\n");
			while ((s = stdInput.readLine()) != null) {
				System.out.println(s);
			}

			// read any errors from the attempted command
			System.out.println("Here is the standard error of the command (if any):\n");
			while ((s = stdError.readLine()) != null) {
				System.out.println(s);
			}
			return true;
		}catch (Exception e) {
			System.out.println("exception happened - here's what I know: ");
			e.printStackTrace();
			return false;
		}
	}

	public static boolean splitPDF(String pdfloc,String pdfname){
		//assumes that there is a directory with the same name as the pdf.
		String newpdfloc=pdfloc.substring(0,pdfloc.length()-4)+"/"+pdfname;
		try{
			FileUtils.copyFile(new File(pdfloc), new File(newpdfloc));
		}catch(IOException e){
			System.out.println("Failed to copy file");
			return false;
		}
		String splitFilebase=newpdfloc.substring(0,newpdfloc.length()-4)+"-%03d.pdf";
		String splitCommand="pdftk "+newpdfloc+" burst output "+splitFilebase;

		String s=null;
		try {
			Process p = Runtime.getRuntime().exec(splitCommand);

			BufferedReader stdInput = new BufferedReader(new
					InputStreamReader(p.getInputStream()));

			BufferedReader stdError = new BufferedReader(new
					InputStreamReader(p.getErrorStream()));

			System.out.println("Here is the standard output of the command:\n");
			while ((s = stdInput.readLine()) != null) {
				System.out.println(s);
			}

			// read any errors from the attempted command
			System.out.println("Here is the standard error of the command (if any):\n");
			while ((s = stdError.readLine()) != null) {
				System.out.println(s);
			}
			return true;
		}catch (Exception e) {
			System.out.println("exception happened - here's what I know: ");
			e.printStackTrace();
			return false;
		}
	}
	public static class GenericExtFilter implements FilenameFilter {

		private String ext;

		public GenericExtFilter(String ext) {
			this.ext = ext;
		}

		public boolean accept(File dir, String name) {
			return (name.endsWith(ext));
		}
	}

	public static List<String> listFile(String folder, String ext) {

		GenericExtFilter filter = new GenericExtFilter(ext);

		File dir = new File(folder);

		if(dir.isDirectory()==false){
			System.out.println("Directory does not exists : " + folder);
			return null;
		}

		// list out all the file name and filter by the extension
		String[] list = dir.list(filter);

		if (list.length == 0) {
			System.out.println("no files end with : " + ext);
			return null;
		}

		List<String> metFiles= new ArrayList<String>();

		for (String file : list) {
			String temp = new StringBuffer(folder).append(File.separator)
					.append(file).toString();
			metFiles.add(temp);
		}
		return metFiles;
	}

	public static void cropImages(String pdfloc){
		BufferedImage originalImg = null;
		List<String> metFiles=listFile(pdfloc.substring(0,pdfloc.length()-4),"met");
		if (metFiles==null)
			return;
		for (String s:metFiles){
			System.out.println(s);
			String figLoc=null;
			String pdPage=null;
			String figId=null;
			List<Integer> cropBox = new ArrayList<Integer>();
			try{
				String sCurrentLine;
				BufferedReader br = new BufferedReader(new FileReader(s));
				while ((sCurrentLine = br.readLine()) != null) {
					if (sCurrentLine.contains("pdpage")){
						pdPage=sCurrentLine.split("<pdpage>")[1].split("</pdpage>")[0].trim();
					}
					if (sCurrentLine.contains("figloc")){
						figLoc=sCurrentLine.split("<figloc>")[1].split("</figloc>")[0].trim();
					}
					if (sCurrentLine.contains("figid")){
						figId=sCurrentLine.split("<figid>")[1].split("</figid>")[0].trim();
					}
					//System.out.println(pdPage+":"+figLoc+":"+figId);
				}
				try{
					for (String s1:figLoc.split(",")){
						//x1,y1,x2,y2: upper left corner x1
						cropBox.add(Integer.parseInt(s1));
					}
				}catch (NumberFormatException e) {
					System.out.println("Number conversion failed "+figLoc);
					continue;
				}
				if (pdPage!=null && figId!=null && cropBox.size()==4){
					System.out.println("Image being cropped from PDF page number "+pdPage);
					String pdPageImageLoc=pdfloc.substring(0,pdfloc.length()-4)+"/"+new File(pdfloc).getName().substring(0,new File(pdfloc).getName().length()-4)+"-"+pdPage+".png";
					String figureLoc=pdfloc.substring(0,pdfloc.length()-4)+"/"+new File(pdfloc).getName().substring(0,new File(pdfloc).getName().length()-4)+"-fig-"+figId+".png";
					//System.out.println("Image being cropped from PDF page number "+pdPage+"::"+pdPageImageLoc+"::"+figureLoc);
					try {
						originalImg = ImageIO.read(new File(pdPageImageLoc));
						//BufferedImage img = 
						//	    JPEGCodec.createJPEGDecoder(new File(pdPageImageLoc)).decodeAsBufferedImage();
						//System.out.println(originalImg.get +originalImg.getHeight());
						ImageProducer croppedImage=new FilteredImageSource(originalImg.
								getSource(),new CropImageFilter(cropBox.get(0), cropBox.get(1), cropBox.get(2)-cropBox.get(0), cropBox.get(3)-cropBox.get(1)));
						//BufferedImage croppedImage = originalImg.getSubimage(cropBox.get(0), cropBox.get(1), cropBox.get(2)-cropBox.get(0), cropBox.get(3)-cropBox.get(1));
						File outputfile = new File(figureLoc);
						Image image = Toolkit.getDefaultToolkit().createImage(croppedImage);
						BufferedImage bufferedImage = new BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_3BYTE_BGR);
						Graphics2D g2 = bufferedImage.createGraphics();
						g2.drawImage(image, 0, 0, null);
						g2.dispose();
						ImageIO.write(bufferedImage, "png", outputfile);
					} catch (Exception e) {
						//e.printStackTrace();
						System.out.println("image cropping failed");
						br.close();
						continue;
					}
				}

				br.close();
			}catch(IOException e){
				System.out.println("Could not open met file: "+s);
				continue;
			}
		}
	}
	/*
	 */
	public static boolean ConvertandCropImages(String pdfloc, String pdfname){
		BufferedImage originalImg = null;
		List<String> metFiles=listFile(pdfloc.substring(0,pdfloc.length()-4),"met");
		if (metFiles==null)
			return false;
		//first split the PDF, takes less time. Then convert pages as necessary
		if (!splitPDF(pdfloc,pdfname))
			return false;
		//String convertCommand="convert "+"-density 150 "+newpdfloc+" -quality 100 -sharpen 0x1.0 "+imageFilebase;
		
		for (String s:metFiles){
			System.out.println(s);
			String figLoc=null;
			String pdPage=null;
			String figId=null;
			List<Integer> cropBox = new ArrayList<Integer>();
			try{
				String sCurrentLine;
				BufferedReader br = new BufferedReader(new FileReader(s));
				while ((sCurrentLine = br.readLine()) != null) {
					if (sCurrentLine.contains("pdpage")){
						pdPage=sCurrentLine.split("<pdpage>")[1].split("</pdpage>")[0].trim();
					}
					if (sCurrentLine.contains("figloc")){
						figLoc=sCurrentLine.split("<figloc>")[1].split("</figloc>")[0].trim();
					}
					if (sCurrentLine.contains("figid")){
						figId=sCurrentLine.split("<figid>")[1].split("</figid>")[0].trim();
					}
					//System.out.println(pdPage+":"+figLoc+":"+figId);
				}
				try{
					for (String s1:figLoc.split(",")){
						//x1,y1,x2,y2: upper left corner x1
						cropBox.add(Integer.parseInt(s1));
					}
				}catch (NumberFormatException e) {
					System.out.println("Number conversion failed "+figLoc);
					continue;
				}
				if (pdPage!=null && figId!=null && cropBox.size()==4){
					if (pdPage.length()==1)
						pdPage="00"+pdPage;
					else if (pdPage.length()==2)
						pdPage="0"+pdPage;
					String temp=pdfloc.substring(0,pdfloc.length()-4)+"/"+pdfname;
					String newpdfloc=temp.substring(0,temp.length()-4)+"-"+pdPage+".pdf";
					String imageFilebase=newpdfloc.substring(0,newpdfloc.length()-4)+".png";
					String convertCommand="convert "+"-density 150 "+newpdfloc+" -quality 100 -sharpen 0x1.0 "+imageFilebase;
					if (!new File(imageFilebase).exists()){
						try {
							Process p = Runtime.getRuntime().exec(convertCommand);

							BufferedReader stdInput = new BufferedReader(new
									InputStreamReader(p.getInputStream()));

							BufferedReader stdError = new BufferedReader(new
									InputStreamReader(p.getErrorStream()));

							System.out.println("Here is the standard output of the command:\n");
							while ((s = stdInput.readLine()) != null) {
								System.out.println(s);
							}

							// read any errors from the attempted command
							System.out.println("Here is the standard error of the command (if any):\n");
							while ((s = stdError.readLine()) != null) {
								System.out.println(s);
							}
						}catch (Exception e) {
							System.out.println("exception happened - here's what I know: ");
							e.printStackTrace();
							continue;
						}
						
					}
					System.out.println("Image being cropped from PDF page number "+pdPage);
					String pdPageImageLoc=imageFilebase;
					String figureLoc=pdfloc.substring(0,pdfloc.length()-4)+"/"+new File(pdfloc).getName().substring(0,new File(pdfloc).getName().length()-4)+"-fig-"+figId+".png";
					//System.out.println("Image being cropped from PDF page number "+pdPage+"::"+pdPageImageLoc+"::"+figureLoc);
					try {
						originalImg = ImageIO.read(new File(pdPageImageLoc));
						//BufferedImage img = 
						//	    JPEGCodec.createJPEGDecoder(new File(pdPageImageLoc)).decodeAsBufferedImage();
						//System.out.println(originalImg.get +originalImg.getHeight());
						ImageProducer croppedImage=new FilteredImageSource(originalImg.
								getSource(),new CropImageFilter(cropBox.get(0), cropBox.get(1), cropBox.get(2)-cropBox.get(0), cropBox.get(3)-cropBox.get(1)));
						//BufferedImage croppedImage = originalImg.getSubimage(cropBox.get(0), cropBox.get(1), cropBox.get(2)-cropBox.get(0), cropBox.get(3)-cropBox.get(1));
						File outputfile = new File(figureLoc);
						Image image = Toolkit.getDefaultToolkit().createImage(croppedImage);
						BufferedImage bufferedImage = new BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_3BYTE_BGR);
						Graphics2D g2 = bufferedImage.createGraphics();
						g2.drawImage(image, 0, 0, null);
						g2.dispose();
						ImageIO.write(bufferedImage, "png", outputfile);
					} catch (Exception e) {
						//e.printStackTrace();
						System.out.println("image cropping failed");
						br.close();
						continue;
					}
				}

				br.close();
			}catch(IOException e){
				System.out.println("Could not open met file: "+s);
				continue;
			}
		}
		return true;
	}
}
