package at.ac.tuwien.dbai.pdfwrap;

import java.io.File;

public class RunProcessFile {
	public static void main(String args[]){
		String inpdir=args[0];
		File folder = new File(inpdir);
		File[] listOfFiles = folder.listFiles();

		    for (int i = 0; i < listOfFiles.length; i++) {
		      if (listOfFiles[i].isFile()) {
		    	  String[] arg= new String[1];
		    	  arg[0]=listOfFiles[i].getAbsolutePath();
		    	  System.out.println(arg[0]);
		    	  if (arg[0].toLowerCase().endsWith("pdf"))
					try {
						System.out.println("------------------------------------------");
						System.out.println("Processing file: "+arg[0]);
						System.out.println("------------------------------------------");
						long start = System.currentTimeMillis();
						long end = start + 60*1000; // 60 seconds * 1000 ms/sec
						boolean notdone=true;
						while ((System.currentTimeMillis() < end)&&notdone)
						{
							notdone=true;
							ProcessFile.main(arg);
							notdone=false;
						}
						
					} catch (Exception e) {
						e.printStackTrace();
						break;
					}
		      } else if (listOfFiles[i].isDirectory()) {
		        System.out.println("Directory " + listOfFiles[i].getName());
		      }
		    }
	}

}
