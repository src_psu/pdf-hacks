package at.ac.tuwien.dbai.pdfwrap;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import at.ac.tuwien.dbai.pdfwrap.model.document.TextBlock;

public class TestKMedoidClustering {
	public static void main(String args[]){
		/*['f1', '120', '65', '506', '483']
		['f2', '508', '71', '730', '479']
		['t1', '277', '489', '592', '510']*/

		/*	f1,(float) 456,(float) 285,(float) 596,(float) 365
			f1,(float) 573,(float) 281,(float) 754,(float) 363
			f1,(float) 449,(float) 334,(float) 615,(float) 474
			f1,(float) 594,(float) 330,(float) 762,(float) 479
			t1,(float) 479,(float) 487,(float) 751,(float) 510
			f2,(float) 458,(float) 764,(float) 535,(float) 972
			f2,(float) 548,(float) 768,(float) 607,(float) 976
			f2,(float) 626,(float) 766,(float) 754,(float) 831
			f2,(float) 619,(float) 852,(float) 762,(float) 976
			t2,(float) 491,(float) 984,(float) 731,(float) 1010 */
		List<List<Float>> figureBoxes=new ArrayList<List<Float>>();
				
		figureBoxes.add(Arrays.asList(new Float []{(float) 626,(float) 766,(float) 754,(float) 831}));
		figureBoxes.add(Arrays.asList(new Float []{(float) 548,(float) 768,(float) 607,(float) 976}));
		figureBoxes.add(Arrays.asList(new Float []{(float) 458,(float) 764,(float) 535,(float) 972}));
		figureBoxes.add(Arrays.asList(new Float []{(float) 456,(float) 285,(float) 596,(float) 365}));
		figureBoxes.add(Arrays.asList(new Float []{(float) 573,(float) 281,(float) 754,(float) 363}));
		figureBoxes.add(Arrays.asList(new Float []{(float) 449,(float) 334,(float) 615,(float) 474}));
		figureBoxes.add(Arrays.asList(new Float []{(float) 594,(float) 330,(float) 762,(float) 479}));
		figureBoxes.add(Arrays.asList(new Float []{(float) 619,(float) 852,(float) 762,(float) 976}));
		
		int nocaption=2;

		List<TextBlock> captions=new ArrayList<TextBlock>();
		TextBlock t1= new TextBlock();
		TextBlock t2= new TextBlock();
		
		t1.setX1(479);t1.setY1(487);t1.setX2(751);t1.setY2(510); //(new float[] {(float) 479,(float) 487,(float) 751,(float) 510});
		t2.setX1(491);t2.setY1(984);t2.setX2(731);t2.setY2(1010);
		//t2.setBoundingBox(new float[] {(float) 491,(float) 984,(float) 731,(float) 1010});
		
		captions.add(t1);
		captions.add(t2);

		int [] clusters = fastKmedoidClustering.fastKMediodClustering(figureBoxes, nocaption, captions);
		for (int i:clusters){
			System.out.println(i);
		}


	}

}
