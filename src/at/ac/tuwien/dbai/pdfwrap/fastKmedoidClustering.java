package at.ac.tuwien.dbai.pdfwrap;

import java.util.ArrayList;
import java.util.List;

import at.ac.tuwien.dbai.pdfwrap.model.document.TextBlock;

public class fastKmedoidClustering {
	public static int[] fastKMediodClustering(List<List<Float>> figureBoxes,
			int nocaption, List<TextBlock> captions) {

		List<List<Float>> textBoxes = new ArrayList<List<Float>>();
		for (int i=0; i<captions.size(); i++){
			List<Float> temp= new ArrayList<Float>();
			TextBlock t = captions.get(i);
			//TODO: check if the x1, y1, x2, y2 s are in the way we see them.  
			//x1, y1, x2, y2
			temp.add(t.getX1());temp.add(t.getY1());temp.add(t.getX2());temp.add(t.getY2());
			textBoxes.add(temp);
		}
		//TODO: uncomment for checking
		//System.out.println(figureBoxes.size()+" "+nocaption+" "+captions.size());
		int [] initialMedoidIndices = chooseInitialPoints(figureBoxes,textBoxes); //these gives us the indices 
		//of the boxes we consider to be the initial points.
		//TODO: uncomment for checking
		//Helper.printArray(initialMedoidIndices);
		int [] clusters=assignBoxestoClusters(initialMedoidIndices,figureBoxes);
		//TODO: uncomment for checking
		//System.out.println("Initial assigment");
		//Helper.printArray(clusters);
		int K=5;//Number of iterations
		List <Float> dist= new ArrayList<Float>(); //sum of distances between cluster centers and points in the cluster
		dist.add((float) 10000);
		float distancethreshold=0; //TODO: this should be good for now, check if need to change.
		//the items assigned to these clusters. 
		int [] clusterCenterIndices=initialMedoidIndices;
		for (int iter=0;iter<K;iter++){
			//TODO: uncomment for checking
			//System.out.println("Iteration number "+iter);
			//System.out.println("Cluster centers");
			//Helper.printArray(clusterCenterIndices);
			//System.out.println("Cluster assignment");
			//Helper.printArray(clusters);

			//TODO:uncomment it for comparisons?
			//dist.add(calculateIntraClusterDistance(clusters,figureBoxes));
			dist.add(calculateIntraClusterDistanceMedoid(clusters,clusterCenterIndices,figureBoxes));
			//TODO: uncomment for checking
			//System.out.println("Intra cluster distance: "+dist.get(dist.size()-1));
			if (Math.abs((dist.get(dist.size()-1)-dist.get(dist.size()-2)))<=distancethreshold) //we have reached the optimal clustering.
				break;
			else { // we need to choose new cluster centers, and then update the clusters
				clusterCenterIndices=chooseNewClusterCenters(clusters,figureBoxes);
				clusters=assignBoxestoClusters(clusterCenterIndices,figureBoxes);
			}
		}
		return clusters;
	}

	private static int[] chooseNewClusterCenters(int[] clusters,
			List<List<Float>> figureBoxes) {
		// A cluster center in a cluster is a box which has minimum distance from
		// all other boxes in that cluster. So, for each cluster, and
		// find out the box in the cluster whioch has the desired property
		// first find how many clusters are there
		int noclusters=0;
		for (int i=0; i< clusters.length; i++){
			if (clusters[i]>noclusters)
				noclusters=clusters[i];
		}
		int [] clusterCenters= new int[noclusters];
		for (int i=1; i<=noclusters; i++){
			int clusterno=i;
			//find which points belong to this cluster
			List<Integer> thisclusterpoints=new ArrayList<Integer>();
			for (int j=0; j<clusters.length; j++){
				if (clusters[j]==clusterno){
					thisclusterpoints.add(j);
				}
			}
			// now we have points, we will see which point is the cluster center.
			// we will just return the index of the central point in the 
			//figureBoxes list
			int thisclustercenter=findThisClusterCenter(thisclusterpoints,figureBoxes);
			clusterCenters[i-1]=thisclustercenter;
		}
		return clusterCenters;
	}

	private static int findThisClusterCenter(List<Integer> thisclusterpoints,
			List<List<Float>> figureBoxes) {
		/*int [] thisclusterIndicesinFigureBox=new int[figureBoxes.size()];*/ 
		//for each bounding box, which is not in this cluster, the distance between
		// that bounding box and other bounding boxes is absurdly high,
		//they never get chosen. For bounding boxes which belong to this clusters,
		//the distance is sum of the distance between this box and other boxes in this cluster.
		// index of the minimum item of the following array should be chosen.
		float [] SumDistanceBetweenPointandOtherPoints=new float[figureBoxes.size()];
		float maxdistance=10000;
		for (int i=0; i< figureBoxes.size();i++ ){
			if (isIn(i,thisclusterpoints)){
				SumDistanceBetweenPointandOtherPoints[i]=calculateSumDistanceBetweenPointandOtherPoints(i,
						thisclusterpoints,figureBoxes);
			}
			else
				SumDistanceBetweenPointandOtherPoints[i]=maxdistance;
		}

		int minindex=0;
		for (int i=0; i<figureBoxes.size();i++){
			if (SumDistanceBetweenPointandOtherPoints[i]<maxdistance){
				maxdistance=SumDistanceBetweenPointandOtherPoints[i];
				minindex=i;
			}
		}
		return minindex;
	}

	private static float calculateSumDistanceBetweenPointandOtherPoints(int i,
			List<Integer> thisclusterpoints, List<List<Float>> figureBoxes) {
		// we need to calculate the distance between point i and all other points which are 
		// in the cluster. 
		List<Float> currentBox=figureBoxes.get(i);
		List<List<Float>> boxesToBeUsed = new ArrayList<List<Float>>();
		for (int j=0; j<thisclusterpoints.size();j++){
			if (thisclusterpoints.get(j)!=i){
				boxesToBeUsed.add(figureBoxes.get(thisclusterpoints.get(j)));
			}
		}
		float SumDistanceBetweenPointandOtherPoints=0;
		for (int j=0; j<boxesToBeUsed.size(); j++){
			SumDistanceBetweenPointandOtherPoints+=calculateDistBetweenBoxes(currentBox,boxesToBeUsed.get(j));
		}
		return SumDistanceBetweenPointandOtherPoints;
	}

	private static boolean isIn(int inp, List<Integer> thisClusterPoints) {
		for (int i=0; i<thisClusterPoints.size(); i++){
			if (thisClusterPoints.get(i)==inp)
				return true;
		}

		return false;
	}

	private static float calculateIntraClusterDistanceMedoid(int[] clusters,
			int[] clusterCenterIndices, List<List<Float>> figureBoxes) {
		// for each of the points, calculate sum of distance between this point and 
		// and the central point in its cluster. 
		//first, for each point i, find it's cluster assignment, which is clusters[i].
		//then find the cluster center for that cluster. for a cluster, the cluster center
		//can be found at clustercenterindices[cluster-1]

		float sumoftotaldistance=0;
		for (int i=0; i<figureBoxes.size(); i++){
			List<Float> thisBox=figureBoxes.get(i);
			//List<List<Float>> thisBoxclustermembers=new ArrayList<List<Float>>();
			int thisBoxclusterno=clusters[i];
			// we will check which boxes are in the cluster of this box. 
			//we will calculate the distance between this box and all such boxes, which will be 
			//added up. 
			List<Float> thisclustercenter= figureBoxes.get(clusterCenterIndices[thisBoxclusterno-1]);
			//TODO: uncomment for checking
			//System.out.println("cluster center for box "+i+" is "+clusterCenterIndices[thisBoxclusterno-1]);
			float d =calculateDistBetweenBoxes(thisBox,thisclustercenter);
			sumoftotaldistance+=d;
			//TODO: uncomment for checking
			//System.out.println("calculating distance between box "+i+" box "+clusterCenterIndices[thisBoxclusterno-1]+" distance: "+d);

		}
		return sumoftotaldistance;
	}

	@SuppressWarnings("unused")
	private static float calculateIntraClusterDistance(int[] clusters,
			List<List<Float>> figureBoxes) {
		// for each of the points, calculate sum of distance between this point and 
		// all of the points in its cluster. 
		//first, for each point i, find it's cluster assignment, which is clusters[i].
		//calculate the distance from point i, the intracluster distance
		float sumoftotaldistance=0;
		for (int i=0; i<figureBoxes.size(); i++){
			List<Float> thisBox=figureBoxes.get(i);
			//List<List<Float>> thisBoxclustermembers=new ArrayList<List<Float>>();
			int thisBoxclusterno=clusters[i];
			// we will check which boxes are in the cluster of this box. 
			//we will calculate the distance between this box and all such boxes, which will be 
			//added up. 
			for (int j=0; j<clusters.length; j++){
				if (clusters[j]==thisBoxclusterno){
					float d =calculateDistBetweenBoxes(thisBox,figureBoxes.get(j));
					sumoftotaldistance+=d;
					//TODO: uncomment for checking
					//System.out.println("calculating distance between box "+i+" box "+j+" distance: "+d);
				}
			}

		}
		return sumoftotaldistance;
	}

	private static int[] assignBoxestoClusters(int[] initialPointIndices,
			List<List<Float>> figureBoxes) {
		int [] clusters=new int[figureBoxes.size()];
		//initially add each point to cluster 0,
		// except for the initial points, for which we know
		// they belong to different clusters. 
		//int clustercount=1; 
		for (int i=0; i<clusters.length; i++){
			if (isInSeq(i,initialPointIndices)!=0){
				clusters[i]=isInSeq(i,initialPointIndices);
				//clustercount++;
			}
			else
				clusters[i]=0;
		}
		//TODO: uncomment for checking
		//Helper.printArray(clusters);
		//now we have to check which point belong to which cluster;
		for (int i=0;i<clusters.length; i++){
			if (clusters[i]==0){//otherwise we know this point's cluster assignment!
				//for each of the existing clusters, check which cluster this point (figurebox) belongs to
				List<Float> figureBox = figureBoxes.get(i);

				float mindist=(float)10000.0; int minindex=0;
				for (int j=0;j<initialPointIndices.length;j++){
					List <Float> intialFigureBox=figureBoxes.get(initialPointIndices[j]);
					float dist=calculateDistBetweenBoxes(figureBox,intialFigureBox);
					if (dist<mindist){
						mindist=dist;
						minindex=j;
					}
				}
				//TODO: uncomment for checking
				//System.out.println("The point "+i+" is closest to "+minindex+" box no: "+initialPointIndices[minindex]);
				clusters[i]=minindex+1;//we originally set the clusters in this way

			}
		}

		return clusters;
	}

	private static int isInSeq(int inp, int[] initialPointIndices) {
		// we will return the index+1 of input in the array, if it exists
		//else we will return 0;
		for (int i=0; i< initialPointIndices.length; i++){
			if (inp==initialPointIndices[i])
				return (i+1);
		}
		return 0;
	}

	@SuppressWarnings("unused")
	private static boolean isIn(int inp, int[] initialPointIndices) {
		for (int i=0; i<initialPointIndices.length; i++){
			if (initialPointIndices[i]==inp)
				return true;
		}

		return false;
	}

	private static  int[] chooseInitialPoints(
			List<List<Float>> figureBoxes, List<List<Float>> textBoxes) {
		int [] initialBoxesIndices =  new int[textBoxes.size()];
		for (int i=0; i<textBoxes.size(); i++){
			List<Float> currentTextBox = textBoxes.get(i);
			Float mindist=(float) 10000.0;//arbitrary large number
			int minindex=0;
			for (int j=0;j<figureBoxes.size(); j++){
				Float dist=calculateDistBetweenBoxes(currentTextBox,figureBoxes.get(j));
				//TODO: uncomment for checking
				//System.out.println("distance between textbox "+i+"figurebox "+j+" is "+dist);
				if (dist<mindist){
					mindist=dist;
					minindex=j;
				}
			}
			initialBoxesIndices[i]=minindex;
		}
		return initialBoxesIndices;
	}

	public static Float calculateDistBetweenBoxes(List<Float> box1,
			List<Float> box2) {
		Float distance=(float) 10000.0;
		float dY1=(float) 0; 
		float dY2=(float) 0;
		float dX1=(float) 0; 
		float dX2=(float) 0;
		float[] Q= new float[box1.size()];
		float[] R= new float[box2.size()];

		for (int i=0; i<Q.length; i++)
			Q[i]=box1.get(i);
		for (int i=0; i<R.length; i++)
			R[i]=box2.get(i);

		if (Q[3]<R[1])
			dY1=R[1]-Q[3];
		if (Q[1]>R[3])
			dY2=Q[1]-R[3];
		if (Q[2]<R[0])
			dX1=R[0]-Q[2];
		if (Q[0]>R[2])
			dX2=Q[0]-R[2];

		distance= dX1+dX2+dY1+dY2;

		return distance;
	}
}
