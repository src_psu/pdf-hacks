package at.ac.tuwien.dbai.pdfwrap;

import java.awt.geom.Rectangle2D;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.util.PDFTextStripperByArea;

import at.ac.tuwien.dbai.pdfwrap.model.document.ImageSegment;
import at.ac.tuwien.dbai.pdfwrap.model.document.LineSegment;
import at.ac.tuwien.dbai.pdfwrap.model.document.Page;
import at.ac.tuwien.dbai.pdfwrap.model.document.RectSegment;
import at.ac.tuwien.dbai.pdfwrap.model.document.TextBlock;

public class Helper {

	static float POINTTOPIXELCONSTANT=(float) 2.083; //point to pixel conversion: divide density paramerter 
	//in convert command (CreateImages.java) by 72

	static void getFigureCaptionLocations(
			float[] pageBoundingBox, String inFile, List<ImageSegment> imageSegments, List<LineSegment> lineSegments,
			List<RectSegment> rectSegments, List<TextBlock> textBlocks,
			List<TextBlock> allTextBlocks, Page resultPage, int currentPage) {

		//List <LineSegment> figureLocations=new ArrayList<LineSegment>(); 
		// a figureLocation is a linesegment
		//first see if the page has any rectangle/lines/images
		//List<List<Float>> figureLocations=new ArrayList<List<Float>>();
		if (rectSegments.size()==0 && imageSegments.size()==0 && lineSegments.size()==0)
			return;
		//we have some rectangles and lines stuff. Let's get some figures. First, estimate 
		//number of captions.
		List<TextBlock> captions=getCaptions(textBlocks);

		int nocaption=captions.size();
		//System.out.println("Number of captions for page "+(currentPage+1)+"is "+nocaption);
		if (nocaption==0) //no captions, we don't need to process further
			return;
		//atleast one caption, gotta do some clustering based on number of figures
		clusterBoxes(pageBoundingBox,inFile, imageSegments,lineSegments,rectSegments,textBlocks,allTextBlocks,resultPage,currentPage,nocaption,captions);
		return;
	}

	private static void clusterBoxes(
			float[] pageBoundingBox, String inFile, List<ImageSegment> imageSegments, List<LineSegment> lineSegments,
			List<RectSegment> rectSegments, List<TextBlock> textBlocks,
			List<TextBlock> allTextBlocks, Page resultPage, int currentPage, int nocaption, List<TextBlock> captions) {

		float thresholdTextDensity=(float) (1*findThresholdTextDensity(allTextBlocks)); //TODO: change this constant?
		//convert each segment into a box. check if the box size is significant. 
		List<List<Float>> boundingBoxes=new ArrayList<List<Float>>();
		boolean checkSize=true;
		for (int i=0; i< imageSegments.size(); i++ ){
			if (imageSegments.get(i).getArea() > 0.005*resultPage.getArea() || checkSize){
				List<Float> temp = new ArrayList<Float>();
				float x1=imageSegments.get(i).getX1();
				float x2=imageSegments.get(i).getX2();
				float y1=imageSegments.get(i).getY1();
				float y2=imageSegments.get(i).getY2();
				if (x1>x2){
					float t=x2;
					x2=x1;
					x1=t;
				}
				if (y2>y1){
					float t=y2;
					y2=y1;
					y1=t;
				}
				temp.add(x1);temp.add(y1);temp.add(x2);temp.add(y2);temp.add((float)0);
				boundingBoxes.add(temp);
				//System.out.println("Added bounding box imagesegment "+temp);
			}
		}
		for (int i=0; i< lineSegments.size(); i++ ){
			if (lineSegments.get(i).getArea() > 0.0025*resultPage.getArea() || checkSize){
				List<Float> temp = new ArrayList<Float>();
				float x1=lineSegments.get(i).getX1();
				float x2=lineSegments.get(i).getX2();
				float y1=lineSegments.get(i).getY1();
				float y2=lineSegments.get(i).getY2();
				if (x1>x2){
					float t=x2;
					x2=x1;
					x1=t;
				}
				if (y2>y1){
					float t=y2;
					y2=y1;
					y1=t;
				}
				temp.add(x1);temp.add(y1);temp.add(x2);temp.add(y2);temp.add((float)1);
				//we have added line segments, some line segments will be noise. may be from a table/etc.
				//discard line segments which have high text density in nearby text blocks
				if (lineSegmentPartofFigure(temp,textBlocks,thresholdTextDensity,pageBoundingBox)){
					boundingBoxes.add(temp);
				}
				else
					System.out.println("line segment discarded "+currentPage);
				//System.out.println("Added bounding box linesegment "+temp);
			}
		}

		for (int i=0; i< rectSegments.size(); i++ ){
			if (rectSegments.get(i).getArea() > 0.0025*resultPage.getArea() || checkSize){
				List<Float> temp = new ArrayList<Float>();
				float x1=rectSegments.get(i).getX1();
				float x2=rectSegments.get(i).getX2();
				float y1=rectSegments.get(i).getY1();
				float y2=rectSegments.get(i).getY2();
				if (x1>x2){
					float t=x2;
					x2=x1;
					x1=t;
				}
				if (y2>y1){
					float t=y2;
					y2=y1;
					y1=t;
				}
				temp.add(x1);temp.add(y1);temp.add(x2);temp.add(y2);temp.add((float)2);
				boundingBoxes.add(temp);
				//System.out.println("Added bounding box rectsegment "+temp);
			}
		}


		//Now we will cluster the boxes using fast-kmedoid. We will initialize with
		//boxes which are closest to the figure locations. But we have to do some preprocessing
		fastKMediodClusteringProcessing(inFile, boundingBoxes,captions,nocaption,textBlocks,allTextBlocks,currentPage, resultPage.getHeight());
		return;
	}

	private static float findThresholdTextDensity(List<TextBlock> textBlocks){
		List<Float> textDensities=new ArrayList<Float>();
		for (TextBlock t:textBlocks){
			//textDensities.add(t.getText().length()/t.getArea());
			//using number of characters, instead of text density
			textDensities.add((float) t.getText().length());
		}
		return findMean(textDensities);
		//return findMedian(textDensities);
	}
	@SuppressWarnings("unused")
	private static boolean lineSegmentPartofFigure(List<Float> line,
			List<TextBlock> textBlocks, float thresholdTextDensity, float[] pageBoundingBox) {
		int index=0, minindex=0, min2ndindex=0, min3rdindex=0; 
		float minvalue=(float)10000.0, min2ndvalue=(float)10000.0, min3rdvalue=(float)10000.0;
		List<Float> textDensities=new ArrayList<Float>();
		List<Float> distlineTextBox=new ArrayList<Float>();
		//if the line segment is too close to the page boundary, discard it
		//TODO:check these thresholds
		float DISCARDCONSTANT=(float)0.15;
		float pagex1=pageBoundingBox[0],pagex2=pageBoundingBox[1],pagey1=pageBoundingBox[2],pagey2=pageBoundingBox[3];
		if (line.get(0)<DISCARDCONSTANT*pagex1) 
			return false;
		else if (line.get(2)>(1-DISCARDCONSTANT)*pagex2)
			return false;
		else if (line.get(1)<DISCARDCONSTANT*pagey1) 
			return false;
		else if (line.get(3)>(1-DISCARDCONSTANT)*pagey2)
			return false;

		for (TextBlock t:textBlocks){
			//textDensities.add(t.getText().length()/t.getArea());
			//using number of characters, instead of text density
			textDensities.add((float) t.getText().length());
			List<Float> temp = new ArrayList<Float>();
			float x1=t.getX1();
			float x2=t.getX2();
			float y1=t.getY1();
			float y2=t.getY2();
			if (x1>x2){
				float t1=x2;
				x2=x1;
				x1=t1;
			}
			if (y2>y1){
				float t1=y2;
				y2=y1;
				y1=t1;
			}
			temp.add(x1);temp.add(y1);temp.add(x2);temp.add(y2);
			distlineTextBox.add(fastKmedoidClustering.calculateDistBetweenBoxes(line, temp));
		}
		for (float f:distlineTextBox){
			if (f<minvalue){
				minvalue=f;
				minindex=index;
			}

			if (f>minvalue && f<min2ndvalue ){
				min2ndvalue=f;
				min2ndindex=index;
			}
			if (f>minvalue && f>min2ndvalue && f<min3rdvalue){
				min3rdvalue=f;
				min3rdindex=index;
			}

			index++;
		}
		//System.out.println(minindex+":"+min2ndindex+":"+min3rdindex);
		//float thresholdTextDensity=findMean(textDensities);
		//float thresholdTextDensity=findMedian(textDensities);
		//System.out.println(thresholdTextDensity+": "+textBlocks.get(minindex).getText()+": "+textBlocks.get(min2ndindex).getText()+": "+textBlocks.get(min3rdindex).getText());
		List <Float> temp1=new ArrayList<Float>();
		temp1.add(textDensities.get(minindex));temp1.add(textDensities.get(min2ndindex));temp1.add(textDensities.get(min3rdindex));
		//++
		//if (findMean(temp1)>thresholdTextDensity)
		//System.out.println(thresholdTextDensity+": "+textBlocks.get(minindex).getText());
		if (textDensities.get(minindex)>thresholdTextDensity) //TODO: important to decide this constant value, why 0.7?	
			return false;
		else
			return true;
	}

	private static float findMean(List<Float> textDensities) {
		// TODO Auto-generated method stub
		float total=0;
		for (float f:textDensities){
			total+=f;
		}
		return total/textDensities.size();
	}

	@SuppressWarnings("unused")
	private static float findMedian(List<Float> textDensities) {
		// TODO Auto-generated method stub

		float [] testDensitiesArr= new float[textDensities.size()];
		int i=0;
		for (float f:textDensities){
			testDensitiesArr[i]=f;
			i++;
		}
		//System.out.println("before");
		//printArray(testDensitiesArr);
		Arrays.sort(testDensitiesArr);
		//System.out.println("after");
		//printArray(testDensitiesArr);
		float median;
		if (testDensitiesArr.length % 2 == 0)
			median = (testDensitiesArr[testDensitiesArr.length/2] + testDensitiesArr[testDensitiesArr.length/2 - 1])/2;
		else
			median = testDensitiesArr[testDensitiesArr.length/2];

		return median;

	}

	private static void fastKMediodClusteringProcessing(
			String inFile, List<List<Float>> boundingBoxes, List<TextBlock> captions,
			int nocaption, List<TextBlock> textBlocks, List<TextBlock> allTextBlocks, int currentPage, float pageheight) {
		// check if no. of caption=1, in that case just merge the elements of bounding
		//boxes, convert to pixel range.
		List<Float> figureLocation=new ArrayList<Float>();
		if (nocaption==1){
			figureLocation =merge(boundingBoxes);
			//figureLocation=mergetextBoxes(figureLocation,textBlocks,pageheight);
			//System.out.println("We have a figure, writing metadata "+captions.get(0).getText());
			createFigureMetadata(inFile, figureLocation,captions.get(0),textBlocks,allTextBlocks,currentPage, pageheight);
		}
		else{//we have multiple figures
			int[] clusterforEachBoundingBox= new int[boundingBoxes.size()];
			//Now we will cluster the boxes using fast-kemedoid. We will initialize with
			//boxes which are closest to the figure locations. the clustering should return a
			//cluster label for each class. 
			clusterforEachBoundingBox=fastKmedoidClustering.fastKMediodClustering(boundingBoxes,nocaption,captions);
			for (int i=1;i<=nocaption;i++){
				List<List<Float>> temp = new ArrayList<List<Float>>();
				for (int j=0;j<boundingBoxes.size();j++){
					if (clusterforEachBoundingBox[j]==i)
						temp.add(boundingBoxes.get(j));
				}
				figureLocation =merge(temp);
				//we have got the figure locations, if some text boxes are not inside, let's get them!
				//figureLocation=mergetextBoxes(figureLocation,textBlocks,pageheight);
				//System.out.println("We have a figure, writing metadata "+captions.get(0).getText());
				createFigureMetadata(inFile, figureLocation,captions.get(i-1),textBlocks,allTextBlocks,currentPage, pageheight);
				System.out.println("elam");
			}
		}
		return;
	}


	@SuppressWarnings("unused")
	private static List<Float> mergetextBoxes(List<Float> figureLocation,
			List<TextBlock> textBlocks, float pageheight) {
		//for each textbox, check if parts of it intersects with the figure bounding box
		//if yes, add the textbox to the figurebox, and chenge the figurebox boundary

		List<List<Float>> temp = new ArrayList<List<Float>>();
		temp.add(figureLocation);
		for (TextBlock t:textBlocks){
			List<Float> tb= new ArrayList<Float>();
			tb.add(t.getX1()); tb.add(t.getY1()); 
			tb.add(t.getX2()); tb.add(t.getY2());

			//a little adjustment if x1>x2 and y1<y2
			if (tb.get(0)>tb.get(2)){
				float temp1=tb.get(2);
				tb.add(2,tb.get(0));
				tb.add(0,temp1);
			}

			if (tb.get(1)<tb.get(3)){
				float temp1=tb.get(3);
				tb.add(3,tb.get(1));
				tb.add(1,temp1);
			}

			//we need to shift the origin (in figurelocation as well as textbox) to use the distance function
			List<Float> t1= new ArrayList<Float>();
			t1.add(tb.get(0));t1.add(pageheight-tb.get(1));t1.add(tb.get(2));t1.add(pageheight-tb.get(3));

			List<Float> t2= new ArrayList<Float>();
			t2.add(figureLocation.get(0));t2.add(pageheight-figureLocation.get(1));t2.add(figureLocation.get(2));t2.add(pageheight-figureLocation.get(3));

			if (fastKmedoidClustering.calculateDistBetweenBoxes(t1, t2)==0){
				temp.add(tb);
			}
		}
		if (temp.size()>1){
			//System.out.println("We got some text blocks to merge");
			figureLocation=merge(temp);
		}

		return figureLocation;
	}

	private static void createFigureMetadata(String inFile, List<Float> figureLocation,
			TextBlock caption, List<TextBlock> textBlocks, List<TextBlock> allTextBlocks, int currentPage, float pageheight) {
		// TODO: We will figure out a way to get figure metadata later. The main problem
		// is that the caption we get as input has all spaces removed. Super funky, eh?
		// for now, we will output 1,2,3 in the list Sagnik has written. 
		// This problem appears to be solved, not extensively tested though.
		//first get the directory where the file is. Assuming linux for now, should be changed.  
		String parentDirName = new File(inFile).getParent();
		String fileName=new File(inFile).getName().split("\\.pd")[0];
		//create a directory here
		File outputdir = new File(parentDirName+"/"+fileName);
		//if (!outputdir.exists()) {
		outputdir.mkdir();
		//}
		//TODO: we might need to change it later.
		//else 
		//	return;



		int figuretextstart=new RegexTestHarness().indexAfterCaptionID(caption.getText());
		String temp="figure null";
		//System.out.println(caption.getText()+" "+figuretextstart);
		try{
			temp=caption.getText().substring(0, figuretextstart);
			//System.out.println("figure found "+temp);
		}catch(Exception E){
			temp="figure null";
		}
		String figureid="def";

		if (temp.toLowerCase().contains("figure"))
			figureid=temp.substring(6).trim();
		else if (temp.toLowerCase().contains("fig ")){//the string must contain fig.
			//System.out.println("the string must contain fig. "+caption.getText());
			figureid=temp.substring(4).trim();
		}
		else {//the string must contain fig.
			//System.out.println("the string must contain fig. "+caption.getText());
			figureid=temp.substring(4).trim();
		}
		//System.out.println("figureid: "+figureid);
		if (figureid.endsWith("."))
			figureid=figureid.substring(0,figureid.length()-1);
		figureid=figureid.replaceAll("[^a-zA-Z0-9\\.]", "");
		String metadatafileloc=parentDirName+"/"+fileName+"/"+fileName+"-fig-"+figureid+".met";
		PrintWriter writer;
		String figureloc="";
		//get the mention for the figure
		List<TextBlock> mentions=getMentions(allTextBlocks,caption.getText(),figureid);

		//we need to get the actual text, not the text from the textblocks because they have
		//no indentation.
		PDFTextStripperByArea stripper=null;
		PDDocumentCatalog thisDoc = null;
		PDPage thisPage=null;
		String captionText="No caption found";
		String mentionText="";
		try {
			thisDoc = PDDocument.load(inFile).getDocumentCatalog();
			thisPage = (PDPage)thisDoc.getAllPages().get(currentPage);
			stripper = new PDFTextStripperByArea();
			Rectangle2D rect= caption.getBoundingRectangle();
			rect.setFrame(rect.getMinX(),pageheight-rect.getMaxY(),rect.getWidth(),rect.getHeight());
			stripper.addRegion("caption",rect);
			stripper.extractRegions(thisPage);
			captionText=stripper.getTextForRegion("caption");
			//System.out.println("the caption is: "+captionText);
			for (TextBlock mentiont:mentions){
				//stripper = new PDFTextStripperByArea();
				rect= mentiont.getBoundingRectangle();
				rect.setFrame(rect.getMinX(),pageheight-rect.getMaxY(),rect.getWidth(),rect.getHeight());
				stripper.addRegion("mention",rect);
				int mentionPageNo=0;
				try {
					mentionPageNo=Integer.parseInt(mentiont.getText().split("</pageno>")[0].split("<pageno>")[1]);
					//System.out.println(mentiont.getText());
				}catch(NumberFormatException e){
					continue;
				}
				thisPage = (PDPage)thisDoc.getAllPages().get(mentionPageNo);
				stripper.extractRegions(thisPage);
				mentionText+=stripper.getTextForRegion("mention");//+"\n-------------------------\n";
			}
			//System.out.println("the mention is: "+mentionText);
			if (mentionText==""){
				mentionText="No mention found";
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}


		// for figure locations, we need to do a scale conversion here. first, 
		// shift origin to top left: subtract every y value from Y page size in points.
		// now we have points, to convert them to pixels, multiply every thing by 96/72 (1.33)
		// we assume here that there is 72 points per inch, and 96 pixels per inch.
		//it is important to convert the PDF to images using the density parameter -density 96
		//see http://stackoverflow.com/questions/139655/convert-pixels-to-points 
		//and http://stackoverflow.com/questions/6605006/convert-pdf-to-image-with-high-resolution 
		//(comment to the accepted answer). 
		figureloc+=(int) (figureLocation.get(0)*POINTTOPIXELCONSTANT)+","; //this is x1, just multiply.
		figureloc+= (int) ((pageheight-figureLocation.get(1))*POINTTOPIXELCONSTANT)+","; //this is y1, subtract from page y first. 
		figureloc+= (int) (figureLocation.get(2)*POINTTOPIXELCONSTANT)+",";//this is x2, just multiply.
		figureloc+= (int) ((pageheight-figureLocation.get(3))*POINTTOPIXELCONSTANT); //this is y2, subtract from page y first.
		//System.out.println("figureloc: "+figureloc);

		//TODO: Following part is to output the figure location in points, for pixels use the above one

		//figureloc+=Math.round(figureLocation.get(0))+","; //this is x1, just multiply.
		//figureloc+= Math.round((pageheight-figureLocation.get(1)))+","; //this is y1, subtract from page y first. 
		//figureloc+= Math.round(figureLocation.get(2))+",";//this is x2, just multiply.
		//figureloc+= Math.round((pageheight-figureLocation.get(3))); //this is y2, subtract from page y first.

		try {
			//System.out.println("writing to "+metadatafileloc);
			writer = new PrintWriter(metadatafileloc, "UTF-8");
			writer.println("<pdloc> "+inFile+" </pdloc>\n");
			writer.println("<pdpage> "+(currentPage+1)+" </pdpage>\n"); //if we use ConvertandCropImages instead of cropImages in 
			//CreateImages.java. Else use currentPage instead of currentPage+1
			writer.println("<figloc> "+figureloc+" </figloc>\n");
			writer.println("<figid> "+figureid+" </figid>\n");
			writer.println("<caption> "+captionText+" </caption>\n");
			writer.println("<mention> "+mentionText+" </mention>\n");
			writer.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	}

	private static List<Float> merge(List<List<Float>> boundingBoxes) {
		//to merge a bunch of rectangles. the origin is bottom left, not the usual 
		//top left. So, our x1=min(X1), y1=max(Y1), x2=max(X2), y2=min(Y2)
		//i=0=x1, i=1=y1,i=2=x2, i=3=y2

		//if a bounding box to be merged contains an image segment/ rect segment, and merging produces
		//a very big box, we will just return the image segment/ rect segment.
		//boolean returnMerged=true;
		//List<Float> imSegment= new ArrayList<Float>();
		List<List<Float>> tempboundingBoxes= new ArrayList(boundingBoxes);
		//imSegment.add((float)0);imSegment.add((float)0);imSegment.add((float)0);imSegment.add((float)0);
		//float maxImSegArea=getAreaBoundingBox(imSegment);
		/*
		for (List<Float> b: boundingBoxes){
			if ((b.get(4)==(float)0)&&(getAreaBoundingBox(b)>maxImSegArea)){
				imSegment=b;
				maxImSegArea=getAreaBoundingBox(b);
				//System.out.println("got an image "+maxImSegArea+" "+b);
			}
		}
		*/
		for (List<Float> b: tempboundingBoxes){
			if (b.get(4)==(float)0){
				float bheight=Math.abs(b.get(1)-b.get(3));
				float bwidth=Math.abs(b.get(2)-b.get(0));
				for (List<Float> b1: tempboundingBoxes){
					float tempDist=fastKmedoidClustering.calculateDistBetweenBoxes(b, b1);
					if ((b1.get(4)==(float)1)&&((tempDist>bheight)||(tempDist>bwidth))){//this is a line segment which is way too far
						boundingBoxes.remove(b1);
					}
				}
			}
		}
		Float[] x1arr=new Float[boundingBoxes.size()];
		Float[] y1arr=new Float[boundingBoxes.size()];
		Float[] x2arr=new Float[boundingBoxes.size()];
		Float[] y2arr=new Float[boundingBoxes.size()];
		for (int i=0; i<boundingBoxes.size(); i++){
			x1arr[i]=boundingBoxes.get(i).get(0);
			y1arr[i]=boundingBoxes.get(i).get(1);
			x2arr[i]=boundingBoxes.get(i).get(2);
			y2arr[i]=boundingBoxes.get(i).get(3);
		}
		List<Float> mergedBox= new ArrayList<Float>();
		mergedBox.add(findMin(x1arr));mergedBox.add(findMax(y1arr));
		mergedBox.add(findMax(x2arr));mergedBox.add(findMin(y2arr));
		//System.out.println("mergedbox area "+getAreaBoundingBox(mergedBox));
		//if (getAreaBoundingBox(mergedBox)>2*maxImSegArea){
			//System.out.println("returning just image");
			//return imSegment;
		//}
		//System.out.println("-------------\n");
		//System.out.println(findMin(new Float[]{(float) 9,(float)7,(float)15,(float)2}));
		//System.out.println(findMax(new Float[]{(float) 9,(float)7,(float)15,(float)2}));
		//System.out.println("mergedbox");
		//for(float i:mergedBox){
		//	System.out.println(i);
		//}
		//System.out.println("---------------\n");
		//else
			return mergedBox;
	}

	@SuppressWarnings("unused")
	private static float getAreaBoundingBox(List<Float> imSegment) {
		// TODO Auto-generated method stub
		//System.out.println("imSegment inside "+imSegment);
		float t1=imSegment.get(2)-imSegment.get(0);
		float t2=imSegment.get(1)-imSegment.get(3);
		return (t1*t2);
	}

	private static Float findMin(Float[] arr) {
		float minv=(float) 10000.1;
		for (int i=0; i<arr.length; i++){
			if (arr[i]<minv)
				minv=arr[i];
		}
		return minv;	
	}

	private static Float findMax(Float[] arr) {
		float maxv=(float) 0;
		for (int i=0; i<arr.length; i++){
			if (arr[i]>maxv)
				maxv=arr[i];
		}
		return maxv;	
	}

	private static List<TextBlock> getCaptions(List<TextBlock> textBlocks) {

		//	int nocaption=0;
		List<TextBlock> captions=new ArrayList<TextBlock>();
		for (int i=0;i<textBlocks.size();i++){
			TextBlock t=textBlocks.get(i);
			String c=t.getText().trim();
			if (c.startsWith("Fig")){

				//calculate the index of first capital letter character
				//System.out.println("I found something possible "+c);
				int firstcap=10;
				for (int j=1;j<c.length();j++){
					if(Character.isUpperCase(c.charAt(j))){
						firstcap=j;
						break;
					}
				}
				if (firstcap<15){
					//System.out.println("yes right "+firstcap);
					captions.add(t); //we found a caption
				}
			}
			else if (c.startsWith("FIG.")){
				//calculate the index of first capital letter character
				//System.out.println("I found something possible "+c);
				int firstcap=10;
				for (int j=4;j<c.length();j++){
					if(Character.isUpperCase(c.charAt(j))){
						firstcap=j;
						break;
					}
				}
				if (firstcap<10){
					//System.out.println("yes right "+firstcap);
					captions.add(t); //we found a caption
				}
			}
			else if (c.startsWith("FIGURE")){
				//calculate the index of first capital letter character
				//System.out.println("I found something possible "+c);
				int firstcap=10;
				for (int j=6;j<c.length();j++){
					if(Character.isUpperCase(c.charAt(j))){
						firstcap=j;
						break;
					}
				}
				if (firstcap<10){
					//System.out.println("yes right "+firstcap);
					captions.add(t); //we found a caption
				}
			}
		}
		return captions;
	}

	private static List<TextBlock> getMentions(List<TextBlock> textBlocks, String caption, String ID) {

		//	int nocaption=0;
		List<TextBlock> mentions=new ArrayList<TextBlock>();
		for (TextBlock t:textBlocks){
			//System.out.println("TextBox: "+t.getText());
			if (new RegexTestHarness().CheckforMention(ID,t.getText().toLowerCase())&&!t.getText().split("</pageno>")[1].equals(caption)){
				//System.out.println("Our mention is: "+t.getText());
				mentions.add(t);
			}
		}
		return mentions;

	}

	public static void printArray(int [] arr){
		System.out.println("------------------");
		for (int i:arr){
			System.out.println(i);
		}
		System.out.println("------------------");
	}

	public static void printArray(float [] arr){
		System.out.println("------------------");
		for (float i:arr){
			System.out.println(i);
		}
		System.out.println("------------------");
	}
}	

