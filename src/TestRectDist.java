import java.util.ArrayList;

import at.ac.tuwien.dbai.pdfwrap.fastKmedoidClustering;


public class TestRectDist {
	public static void main(String args[]){
		ArrayList<Float> R1= new ArrayList<Float>();
		//R1.add((float)219);R1.add((float)783);R1.add((float)223);R1.add((float)1134);
		R1.add((float)104.74);R1.add((float)390.62);R1.add((float)119.72);R1.add((float)386.62);
		ArrayList<Float> R2= new ArrayList<Float>();
		R2.add((float)104.5);R2.add((float)416.72);R2.add((float)108.5);R2.add((float) 246.46002);
		System.out.println(fastKmedoidClustering.calculateDistBetweenBoxes(R1,R2));
	}

}
